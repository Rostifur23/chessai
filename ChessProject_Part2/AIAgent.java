import java.util.*;

public class AIAgent{
  Random rand;

  public AIAgent(){
    rand = new Random();
  }

/*
  The method randomMove takes as input a stack of potential moves that the AI agent
  can make. The agent uses a rondom number generator to randomly select a move from
  the inputted Stack and returns this to the calling agent.
*/

  public Move randomMove(Stack possibilities){

    int moveID = rand.nextInt(possibilities.size());
    System.out.println("Agent randomly selected move : "+moveID);
    for(int i=1;i < (possibilities.size()-(moveID));i++){
      possibilities.pop();
    }
    Move selectedMove = (Move)possibilities.pop();

    return selectedMove;
  }






  public Move nextBestMove(Stack possibilities, Stack pieces){
    Move selectedMove = new Move();
    Square tmp = new Square();
    Stack black = new Stack();
    Move currentMove = new Move();
    int score = 0;
    Boolean noTakes = false;
    Stack movesForRand = (Stack) possibilities.clone();

    while (!possibilities.empty()) {
      black = (Stack) pieces.clone();
      currentMove = (Move) possibilities.pop();

      while(!black.empty()){
        tmp = (Square) black.pop();
        String piece = tmp.getName();
        if((currentMove.getLanding().getXC() == tmp.getXC()) && (currentMove.getLanding().getYC() == tmp.getYC())){
          if(piece.contains("Pawn")){
            if(1 > score){
              score = 1;
              selectedMove = currentMove;
              noTakes = false;
            }
          } else if(piece.contains("Bishop")){
            if(3 > score){
              score = 3;
              selectedMove = currentMove;
              noTakes = false;
            }
          } else if(piece.contains("Rook")){
            if(5 > score){
              score = 5;
              selectedMove = currentMove;
              noTakes = false;
            }
          } else if(piece.contains("Knight")){
            if(4 > score){
              score = 4;
              selectedMove = currentMove;
              noTakes = false;
            }
          } else if(piece.contains("Queen")){
            if(9 > score){
              score = 9;
              selectedMove = currentMove;
              noTakes = false;
            }
          } else if(piece.contains("King")){
            if(1000 > score){
              score = 1000;
              selectedMove = currentMove;
              noTakes = false;
            }
          }
        } else{
          if(score == 0){
            noTakes = true;
          }
        }
      }

    }
    if(noTakes){
      selectedMove = (Move) randomMove(movesForRand);
    }
    return selectedMove;
  }







  public Move twoLevelsDeep(Stack wPossibilities, Stack bPossibilites, Stack blackPieces, Stack whitePieces){
    Move selectedMove = new Move();
    Move move = new Move();
    Stack bRespMoves = new Stack();
    Stack whiteCopy = (Stack) whitePieces.clone();
    Stack blackCopy = (Stack) blackPieces.clone();
    Stack blackCopy1 = (Stack) blackPieces.clone();



    // calculating scores and difference
    int bScore = getScore(blackCopy);
    int wScore = getScore(whiteCopy);
    int difference = (bScore - wScore);

    System.out.println("Black Score: "+bScore);
    System.out.println("White Score: "+wScore);
    System.out.println("Difference: "+difference);

    // applying utility function on moves for black pieces
    Move bestBlackMove = (Move) nextBestMove(bPossibilites, whitePieces);
    System.out.println("Best Option for black is From: "+bestBlackMove.getStart().getXC()+","+bestBlackMove.getStart().getYC()+" -> To: "+bestBlackMove.getLanding().getXC()+","+bestBlackMove.getLanding().getYC());

    /* PSEUDOCODE FOR MY CONCEPT ON HOW TO APPROACH MINIMAX ALGORITHM

      Stack wPoss = (Stack) wPossibilities.clone();
      Stack bPoss = (Stack) bPossibilites.clone();
      int bEffScore = 0;
      int wEffScore = 0;
      Square bPieceTaken = new Square();
      Square wPieceTaken = new Square();

      Move whiteMove = new Move();
      int maxValue = 0;

      while(!(wPoss.empty())){
        Stack bPcs = (Stack) blackPieces.clone();
        int minValue = 0;
        whiteMove = (Move) wPoss.pop();
        while(!(bPcs.empty())){
          Square bp = (Square) bPcs.pop();
          if((bp.getXC() == whiteMove.getLanding().getXC())&&(bp.getYC() == whiteMove.getLanding().getYC())){
            bPieceTaken = (Square) bp;
          }
        }

        while(!(bPoss.empty())){
          Stack wPcs = (Stack) whitePieces.clone();
          Move blackMove = (Move) bPoss.pop();
          while(!(wPcs.empty())){
            Square wp = (Square) wPcs.pop();
            if((wp.getXC() == blackMove.getLanding().getXC())&&(wp.getYC() == blackMove.getLanding().getYC())){
              wPieceTaken = (Square) wp;
            }
          }

          String bName = bPieceTaken.getName();
          int bDeduct = 0;
          if(bName.contains("Pawn")){
            bDeduct = 1;
          } else if(bName.contains("Knight")){
            bDeduct = 4;
          } else if(bName.contains("Bishop")){
            bDeduct = 5;
          } else if(bName.contains("Rook")){
            bDeduct = 5;
          } else if(bName.contains("Queen")){
            bDeduct = 9;
          }

          String wName = wPieceTaken.getName();
          int wDeduct = 0;
          if(wName.contains("Pawn")){
            wDeduct = 1;
          } else if(wName.contains("Knight")){
            wDeduct = 4;
          } else if(wName.contains("Bishop")){
            wDeduct = 5;
          } else if(wName.contains("Rook")){
            wDeduct = 5;
          } else if(wName.contains("Queen")){
            wDeduct = 9;
          }

          bEffScore = bScore - bDeduct;
          wEffScore = wScore - wDeduct;
          difference = wEffScore - bEffScore;

          if(difference < minValue){
            minValue = difference;
          }

        }

        if(minValue > maxValue){
          maxValue = minValue;
          selectedMove = whiteMove;
        }

      }


      */

      /*
      For Each possible white move
        int minValue = 0;
        whiteMove = get current white move
        Square bPieceTaken = get piece taken by white move

        For each possible black move
          Square wPieceTaken = get piece taken by black move

          blackEffectedScore = blackScore - bPieceTaken
          whiteEffectedScore = whiteScore - wPieceTaken
          difference = whiteEffectedScore - blackEffectedScore

          if (difference < minValue) {
            minValue = difference
          }
        End of black move loop

        if(minValue > maxValue){
          maxValue = minValue
          selectedMove = whiteMove
        }

      End of white move loop

      return selectedMove



    */

    // returns utility for nextBestMove()
    selectedMove = (Move) nextBestMove(wPossibilities, blackCopy1);
    return selectedMove;
  }




// getting overall score for a player
  public int getScore(Stack pieces){
    int score = 0;
    Square s = new Square();
    while(!(pieces.empty())){
      s = (Square) pieces.pop();
      String name = s.getName();
      if(name.contains("Pawn")){
        score = score + 1;
      } else if(name.contains("Knight")){
        score = score + 4;
      } else if(name.contains("Bishop")){
        score = score + 3;
      } else if(name.contains("Rook")){
        score = score + 5;
      } else if(name.contains("Queen")){
        score = score + 9;
      }
    }
    return score;
  }

}
